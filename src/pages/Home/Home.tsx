import {View, Button, Text} from 'react-native';
import React from 'react';
import {PageTitle} from '../../components/PageTitle';
import styles from './Home.styles';
import {useFBAuth} from '../../providers/useFbAuth';

export const Home: React.FunctionComponent = () => {
  const {profile, login, logout} = useFBAuth();

  return (
    <View>
      <PageTitle title="Home Page" />
      <View style={styles.contentCentered}>
        {!profile.isAuthenticated ? (
          <Button onPress={login} title="Login with FB" />
        ) : null}
        {profile.isAuthenticated ? (
          <Button onPress={logout} title="Logout" />
        ) : null}
        <View style={styles.messageContainer}>
          <Text>Authentication output:</Text>
          <Text style={styles.message}>{profile.message}</Text>
        </View>
      </View>
    </View>
  );
};
