/**
 * @format
 */

import 'react-native';
import React from 'react';
import {Home} from '../home';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

jest.mock('../../providers/useFbAuth', () => ({
  useFBAuth: () => ({
    profile: {isAuthenticated: false, isLoading: false},
    login: jest.fn(),
    logout: jest.fn(),
  }),
}));

describe('Home component', () => {
  it('renders correctly', () => {
    // useFBAuthMock.mockReturnValue({
    //   profile: {},
    //   login: jest.fn(),
    //   logout: jest.fn(),
    // });
    renderer.create(<Home />);
  });
});
