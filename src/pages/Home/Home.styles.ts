import {StyleSheet} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const styles = StyleSheet.create({
  contentCentered: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
  },

  messageContainer: {
    marginTop: 40,
  },

  message: {
    marginTop: 12,
    padding: 8,
    backgroundColor: '#e4f1fe',
  },
});

export default styles;
