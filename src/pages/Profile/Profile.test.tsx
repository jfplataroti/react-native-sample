/**
 * @format
 */

import 'react-native';
import React from 'react';
import {Profile} from './Profile';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';

jest.mock('../../providers/useFbAuth', () => ({
  useFBAuth: () => ({
    profile: {isAuthenticated: false, isLoading: false},
    login: jest.fn(),
    logout: jest.fn(),
  }),
}));

describe('Home component', () => {
  it('renders correctly', () => {
    renderer.create(<Profile />);
  });
});
