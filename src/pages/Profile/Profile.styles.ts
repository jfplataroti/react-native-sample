import {StyleSheet} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

const styles = StyleSheet.create({
  title: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
