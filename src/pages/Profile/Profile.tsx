import {View, Text} from 'react-native';
import React from 'react';

import {PageTitle} from '../../components/PageTitle';
import {useFBAuth} from '../../providers/useFbAuth';

export const Profile: React.FunctionComponent = () => {
  const {profile} = useFBAuth();
  return (
    <>
      <PageTitle title="Profile" />
      {profile.isAuthenticated ? (
        <View>
          <Text>User: {profile.profile?.name}</Text>
        </View>
      ) : null}

      {!profile.isAuthenticated ? (
        <View>
          <Text>Unauthenticated User</Text>
        </View>
      ) : undefined}
    </>
  );
};
