import {View, Text} from 'react-native';
import React from 'react';

import styles from './PageTitle.styles';

export interface PageTitleProps {
  title: string;
}

export const PageTitle: React.FunctionComponent<PageTitleProps> = ({title}) => (
  <View style={styles.title}>
    <Text>{title}</Text>
  </View>
);
