import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  title: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
