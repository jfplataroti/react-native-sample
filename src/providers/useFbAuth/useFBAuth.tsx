import React, {useReducer} from 'react';
import {
  LoginManager,
  LoginResult,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';

import {
  authReducer,
  AuthDispatch,
  AuthState,
  initialState,
} from './FBAuthReducer';
import {AuthActions, profilePayload} from './AuthActions';

export type AuthContextValue =
  | {
      profile: AuthState;
      login: () => void;
      logout: () => void;
    }
  | undefined;

const FBAuthContext = React.createContext<AuthContextValue>(undefined);

const loadProfile = (dispatch: AuthDispatch) => {
  const infoRequest = new GraphRequest(
    '/me',
    null,
    (error?: object, result?: object) => {
      if (error) {
        return dispatch(AuthActions.loadProfileFailed(error.toString()));
      }

      return dispatch(AuthActions.loadProfile(result as profilePayload));
    },
  );
  new GraphRequestManager().addRequest(infoRequest).start();
};

const logout = (dispatch: AuthDispatch) => () => {
  LoginManager.logOut();
  dispatch(AuthActions.logout());
};

const login = (dispatch: AuthDispatch) => async () => {
  try {
    dispatch(AuthActions.login());
    const result: LoginResult = await LoginManager.logInWithPermissions([
      'public_profile',
    ]);

    if (result.isCancelled) {
      return dispatch(AuthActions.loginCancelled());
    }

    dispatch(AuthActions.loginSucceded());
    loadProfile(dispatch);
  } catch (e) {
    //missing payload, didn't want to spend to much time in code sample, just added to give you an idea
    dispatch(AuthActions.loginFailed());
  }
};

const FBAuthProvider: React.FunctionComponent = ({children}) => {
  const [profile, dispatch] = useReducer(authReducer, initialState);
  const value = {profile, login: login(dispatch), logout: logout(dispatch)};
  return (
    <FBAuthContext.Provider value={value}>{children}</FBAuthContext.Provider>
  );
};

function useFBAuth() {
  const context = React.useContext(FBAuthContext);
  if (context === undefined) {
    throw new Error('useFBAuth must be used within a FBAuthProvider');
  }
  return context;
}

export {FBAuthProvider, useFBAuth};
