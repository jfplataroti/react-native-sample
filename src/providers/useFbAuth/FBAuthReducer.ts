import {ActionTypes} from './AuthActions';

export type AuthDispatch = (action: ActionTypes) => void;

export interface AuthState {
  isAuthenticated: boolean;
  isLoading: boolean;
  message?: string;
  profile?: {
    id: string;
    name: string;
  };
}

export const initialState: AuthState = {
  isLoading: false,
  isAuthenticated: false,
};

export const authReducer = (
  state: AuthState,
  action: ActionTypes,
): AuthState => {
  console.log('auth reducer action ->', action);
  switch (action.type) {
    case '@fb-auth/login': {
      return {...state, isLoading: true};
    }
    case '@fb-auth/logout': {
      return {isAuthenticated: false, isLoading: false, message: 'logged out'};
    }
    case '@fb-auth/login-succesfull':
      return {
        ...state,
        isAuthenticated: true,
        isLoading: false,
        message: 'Welcome!!!',
      };
    case '@fb-auth/login-cancelled':
      return {
        ...state,
        isLoading: false,
        isAuthenticated: false,
        message: 'login cancelled',
      };
    case '@fb-auth/login-failed':
      return {
        ...state,
        message: 'Login failed',
      };

    case '@fb-auth/loadProfile':
      console.log(action);
      return {
        ...state,
        profile: action.payload,
      };

    case '@fb-auth/loadProfileFailed':
      return {
        ...state,
        message: action.payload,
      };
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
};
