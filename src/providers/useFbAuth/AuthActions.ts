// in production code I would used this library:
// https://github.com/piotrwitek/react-redux-typescript-guide
// is much better and standarized.

export type ActionTypes =
  | {type: '@fb-auth/login'}
  | {type: '@fb-auth/logout'}
  | {type: '@fb-auth/login-cancelled'}
  | {type: '@fb-auth/login-failed'}
  | {type: '@fb-auth/login-succesfull'}
  | {type: '@fb-auth/loadProfile'; payload: profilePayload}
  | {type: '@fb-auth/loadProfileFailed'; payload: string};

export interface profilePayload {
  id: string;
  name: string;
}

export interface Actions {
  loadProfile: (data: profilePayload) => ActionTypes;
  loadProfileFailed: (data: string) => ActionTypes;
  login: () => ActionTypes;
  logout: () => ActionTypes;
  loginCancelled: () => ActionTypes;
  loginSucceded: () => ActionTypes;
  loginFailed: () => ActionTypes;
}

export const AuthActions: Actions = {
  login: () => ({type: '@fb-auth/login'}),
  loadProfile: (data: profilePayload) => ({
    type: '@fb-auth/loadProfile',
    payload: data,
  }),
  loadProfileFailed: (error: string) => ({
    type: '@fb-auth/loadProfileFailed',
    payload: error,
  }),
  logout: () => ({type: '@fb-auth/logout'}),
  loginCancelled: () => ({type: '@fb-auth/login-cancelled'}),
  loginSucceded: () => ({
    type: '@fb-auth/login-succesfull',
  }),
  loginFailed: () => ({
    type: '@fb-auth/login-failed',
  }),
};
