import React from 'react';
import {SafeAreaView, View, Text} from 'react-native';

import {NativeRouter, Route, Link} from 'react-router-native';

import {styles} from './App.styles';
import {Profile} from './pages/Profile';
import {Home} from './pages/Home';
import {FBAuthProvider} from './providers/useFbAuth/useFBAuth';

declare const global: {HermesInternal: null | {}};

const App = () => {
  return (
    <FBAuthProvider>
      <NativeRouter>
        <SafeAreaView>
          <View style={styles.container}>
            <View style={styles.nav}>
              <Link to="/" underlayColor="#f0f4f7" style={styles.navItem}>
                <Text>Home</Text>
              </Link>
              <Link
                to="/profile"
                underlayColor="#f0f4f7"
                style={styles.navItem}>
                <Text>Profile</Text>
              </Link>
            </View>
            <Route exact path="/" component={Home} />
            <Route path="/profile" component={Profile} />
          </View>
        </SafeAreaView>
      </NativeRouter>
    </FBAuthProvider>
  );
};

export default App;
