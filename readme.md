# react native sample

Sample of code. I ended up adding a bit of more code, abstracting the FB authentication into a hook / react context to be able to share the data between pages and to show some custom code to be avaluated :).

## how to run it

`yarn` to install dependencies

`cd ios and pod install` to install native dependencies

`yarn ios` to run emulator.

## Code structure

followed kind of approach used by `nextjs` having routes under pages folder.

```

├── src             // contains code
│   ├── pages       // contains pages, mainly components that are used in routes.
    |   ├── Home
    |   └── Profile
    |
    ├── components // components reused in multiple components.
    └── providers  // contains providers, for example for react contexts
        └── useFBAush  // hook to add FB authentication in the app.
```


### Notes
**Broken tests**: Not all the tests are working, sorry it's just I got carried away with the code and then I realized some weren't working. I hope it still works as sample code.

**React native plugin used**: https://github.com/facebook/react-native-fbsdk

**Use react context as a provider:** Links used to wrap a react context into a hook: https://kentcdodds.com/blog/how-to-use-react-context-effectively